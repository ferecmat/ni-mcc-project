#pragma once

#include <chrono>
#include <iostream>
#include <ostream>

#include "printer.h"

constexpr auto now = std::chrono::system_clock::now;

class Timer {
  public:
    Timer() : Timer("") {}
    Timer(std::string_view tag)
        : _tag(tag), _start(std::chrono::system_clock::now()) {}
    ~Timer() {
        auto end = std::chrono::system_clock::now();
        print(_tag, std::chrono::duration_cast<std::chrono::microseconds>(
                        end - _start)
                        .count());
    }

  private:
    std::string_view _tag{};
    std::chrono::time_point<std::chrono::system_clock> _start{};
};

class times {
  public:
    void add(double init, double counting, double summing, double ordering) {
        _init += init;
        _counting += counting;
        _summing += summing;
        _ordering += ordering;
    }

  private:
    friend std::ostream &operator<<(std::ostream &os, const times &times) {
        return os << times._init << " " << times._counting << " "
                  << times._summing << " " << times._ordering << " "
                  << times._init + times._counting + times._summing +
                         times._ordering;
    }
    double _init;
    double _counting;
    double _summing;
    double _ordering;
};
