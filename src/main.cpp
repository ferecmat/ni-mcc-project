#include <algorithm>
#include <bitset>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <execution>
#include <random>
#include <string_view>
#include <tuple>
#include <vector>

#include <omp.h>

#include "par_sort.h"
#include "printer.h"
#include "seq_sort.h"
#include "timer.h"

typedef uint32_t u32;
typedef uint64_t u64;
typedef int32_t i32;
typedef size_t u;

using sort_func =
    std::function<std::tuple<std::vector<u32>, times>(std::vector<u32>, int)>;

u64 round_up(u64 num, u64 multiple) {
    return ((num + multiple - 1) / multiple) * multiple;
}

std::vector<u32> par_get(u64 size, u32 seed) {
    std::mt19937 gen(seed);
    std::vector<u32> vec(size);

    auto thread_count = static_cast<u32>(omp_get_max_threads());
    auto seeds = std::vector<u64>(thread_count);

    for (u32 i = 0; i < thread_count; i++) {
        seeds[i] = gen();
    }
#pragma omp parallel
    {
        std::uniform_int_distribution<u32> dist{};
        auto g = std::mt19937(seeds[omp_get_thread_num()]);
#pragma omp for
        for (u64 i = 0; i < size; i++) {
            vec[i] = dist(g);
        }
    }
    return vec;
}

std::vector<std::pair<sort_func, sort_func>> sorts() {
    std::vector<std::pair<sort_func, sort_func>> funcs = {
        {seq::sort<1>, par::sort<1>},   {seq::sort<2>, par::sort<2>},
        {seq::sort<4>, par::sort<4>},   {seq::sort<8>, par::sort<8>},
        {seq::sort<16>, par::sort<16>},
    };
    return funcs;
}

void measure(std::vector<u32> vec, sort_func seq, sort_func par,
             int thread_count) {
    {
        auto t = Timer{"\tseq"};
        auto [s, tt] = seq(vec, thread_count);
        print("seq cummulative", tt);
    }
    {
        auto t = Timer{"\tpar"};
        auto [s, tt] = par(vec, thread_count);
        print("par cummulative", tt);
    }
}

int main(int argc, const char *argv[]) {
    std::random_device rd;

    u64 size = 16;
    u32 seed = rd();
    std::string_view sort_type;
    int max_threads = omp_get_max_threads();

    switch (argc) {
    case 2:
        sort_type = argv[1];
        break;
    case 3:
        sort_type = argv[1];
        seed = static_cast<u32>(std::stoull(argv[2]));
        break;
    case 4:
        sort_type = argv[1];
        seed = static_cast<u32>(std::stoul(argv[2]));
        size = round_up(std::stoull(argv[3]),
                        static_cast<u64>(omp_get_max_threads()));
        break;
    case 5:
        sort_type = argv[1];
        seed = static_cast<u32>(std::stoul(argv[2]));
        size = round_up(std::stoull(argv[3]),
                        static_cast<u64>(omp_get_max_threads()));
        max_threads = std::atoi(argv[4]);
        break;
    case 1:
    default:
        std::cerr
            << "usage: ./radix-sort <sort-type:all|1|2|4|8|16> "
               "<seed:int=random> <size:int=16> <max-threads:int=nproc>\n";
        return 1;
    }

    auto funcs = sorts();
    auto radix_sizes = {"1", "2", "4", "8", "16"};
    print("seed:", seed, "\nsize:", size);
    print("threads:", max_threads);

    std::vector<u32> vec{};
    {
        auto t = Timer{"gen"};
        vec = par_get(size, seed);
    }
    if (sort_type == "all") {
        u32 i = 0;
        for (auto [seq, par] : funcs) {
            print(1 << i);
            measure(vec, seq, par, max_threads);
            i++;
        }
    } else if (auto it =
                   std::find(radix_sizes.begin(), radix_sizes.end(), sort_type);
               it != radix_sizes.end()) {
        auto radix_size = it - radix_sizes.begin();
        print("radix size:", *it);
        auto [seq, par] = funcs[radix_size];
        measure(vec, seq, par, max_threads);
    }
}
