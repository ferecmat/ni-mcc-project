#pragma once

#include <omp.h>
#include <tuple>
#include <vector>

#include "base.h"
#include "printer.h"
#include "timer.h"

namespace seq {

template <u32 radix_size>
std::tuple<std::vector<u32>, times> sort(std::vector<u32> const &vec,
                                         [[maybe_unused]] int thread_count);

template <u32 radix_size>
static std::vector<u32> count_sort(std::vector<u32> const &vec,
                                   u32 const radix_index, times &timings);

template <u32 radix_size>
static std::vector<u32> count_sort(std::vector<u32> const &vec,
                                   u32 const radix_index, times &timings) {
    auto start = omp_get_wtime();

    std::vector<u32> counts(1 << radix_size, 0);
    std::vector<u32> sorted(vec.size(), 0);

    auto init = omp_get_wtime() - start;

    for (u32 elem : vec) {
        counts[extract_bytes<radix_size>(elem, radix_index)]++;
    }

    auto counting = omp_get_wtime() - (start + init);

    for (u32 i = 1; i < counts.size(); i++) {
        counts[i] += counts[i - 1];
    }

    auto summing = omp_get_wtime() - (start + init + counting);

    for (u i = vec.size(); i-- > 0;) {
        auto idx = extract_bytes<radix_size>(vec[i], radix_index);
        auto position = --counts[idx];
        sorted[position] = vec[i];
    }

    auto ordering = omp_get_wtime() - (start + init + counting + summing);

    timings.add(init, counting, summing, ordering);
    return sorted;
}

template <u32 radix_size>
std::tuple<std::vector<u32>, times> sort(std::vector<u32> const &vec,
                                         [[maybe_unused]] int thread_count) {
    auto timings = times{};
    auto out = vec;

    for (u32 radix_index = 0; radix_index * radix_size < NUM_SIZE;
         radix_index++) {
        out = count_sort<radix_size>(out, radix_index, timings);
    }
    return {out, timings};
}
} // namespace seq
