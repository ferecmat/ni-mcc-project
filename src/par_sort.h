#pragma once

#include <algorithm>
#include <numeric>
#include <tuple>
#include <vector>

#include <omp.h>

#include "base.h"
#include "printer.h"
#include "timer.h"

namespace par {

template <u32 radix_size>
std::tuple<std::vector<u32>, times> sort(std::vector<u32> const &vec,
                                         int thread_count);

template <u32 radix_size>
static std::vector<u32> count_sort(std::vector<u32> const &vec,
                                   u32 const radix_index, times &timings,
                                   int thread_count);

template <u32 radix_size>
static std::vector<u32> count_sort(std::vector<u32> const &vec,
                                   u32 const radix_index, times &timings,
                                   int thread_count) {

    auto start = omp_get_wtime();

    u32 sum = 0;
    constexpr u32 counts_size = 1 << radix_size;
    constexpr u32 cache_line_size = 64;
    u32 dummy_fill = cache_line_size / sizeof(u32);

    auto counts = std::vector<std::vector<u32>>(
        counts_size * thread_count, std::vector<u32>(dummy_fill, 0));
    auto counts_summed = std::vector<std::vector<u32>>(
        counts_size * thread_count, std::vector<u32>(dummy_fill, 0));

    std::vector<u32> sorted(vec.size(), 0);

    const u64 job_size = vec.size() / thread_count;

    auto init = omp_get_wtime() - start;

#pragma omp parallel num_threads(thread_count) default(none) shared(counts)    \
    shared(thread_count) shared(job_size) shared(radix_index) shared(vec)
    {
        const u32 thread_num = static_cast<u32>(omp_get_thread_num());
        for (u32 i = 0; i < job_size; i++) {
            u32 idx = extract_bytes<radix_size>(vec[thread_num * job_size + i],
                                                radix_index);
            counts[thread_count * idx + thread_num][0]++;
        }
    }

    auto counting = omp_get_wtime() - (start + init);

#pragma omp simd reduction(inscan, + : sum)
    for (u i = 0; i < counts_size * thread_count; i++) {
        sum += counts[i][0];
#pragma omp scan inclusive(sum)
        counts_summed[i][0] = sum;
    }

    auto summing = omp_get_wtime() - (start + init + counting);

#pragma omp parallel num_threads(thread_count) default(none)                   \
    shared(counts_summed) shared(thread_count) shared(job_size)                \
        shared(radix_index) shared(vec) shared(sorted)
    {
        const u32 thread_num = static_cast<u32>(omp_get_thread_num());
        for (u i = job_size; i-- > 0;) {
            auto idx = extract_bytes<radix_size>(vec[thread_num * job_size + i],
                                                 radix_index);
            u32 position = --counts_summed[thread_count * idx + thread_num][0];
            sorted[position] = vec[thread_num * job_size + i];
        }
    }

    auto ordering = omp_get_wtime() - (start + init + counting + summing);
    timings.add(init, counting, summing, ordering);
    return sorted;
}

template <u32 radix_size>
std::tuple<std::vector<u32>, times> sort(std::vector<u32> const &vec,
                                         int thread_count) {
    auto timings = times{};
    auto out = vec;

    for (u32 radix_index = 0; radix_index * radix_size < NUM_SIZE;
         radix_index++) {
        out = count_sort<radix_size>(out, radix_index, timings, thread_count);
    }

    return {out, timings};
}

} // namespace par
