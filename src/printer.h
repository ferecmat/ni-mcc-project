#pragma once

#include <cstddef>
#include <iostream>
#include <vector>

static void print_(bool const &b);
template <typename T> static void print_(std::vector<T> const &vec);
template <typename Arg> static void print_(Arg const &arg);

template <typename Arg> void print(Arg const &arg);
template <typename Arg, typename... Args>
void print(Arg const &arg, Args const &...args);

template <typename T> static void print_(std::vector<T> const &vec) {
    std::cout << "[";
    size_t i = 0;
    for (; i < vec.size() - 1; i++) {
        print_(vec[i]);
        std::cout << ", ";
    }
    print_(vec[i]);
    std::cout << "]";
}

static void print_(bool const &b) { std::cout << (b ? "true" : "false"); }

template <typename Arg> static void print_(Arg const &arg) { std::cout << arg; }

template <typename Arg> void print(Arg const &arg) {
    print_(arg);
    print_("\n");
}

template <typename Arg, typename... Args>
void print(Arg const &arg, Args const &...args) {
    print_(arg);
    print_(" ");
    print(args...);
}
