#pragma once

#include <cstdint>
#include <cstdlib>
#include <limits>

typedef uint32_t u32;
typedef uint64_t u64;
typedef int32_t i32;
typedef size_t u;

constexpr u NUM_SIZE = 32;

template <u32 radix_size>
concept pow2 = (radix_size & (radix_size - 1)) == 0;

template <u32 radix_size>
u32 extract_bytes(u32 const elem, u32 const radix_index)
    requires pow2<radix_size>;

template <u32 radix_size>
u32 extract_bytes(u32 const elem, u32 const radix_index)
    requires pow2<radix_size>
{
    u32 a = (elem >> (radix_index * radix_size));
    u32 b = (std::numeric_limits<u32>::max() >> (NUM_SIZE - radix_size));
    return a & b;
}
