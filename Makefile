.PHONY: clean run

radix-sort: src/main.cpp src/par_sort.h src/seq_sort.h src/base.h src/printer.h
	g++ -O3 -march=broadwell -fopenmp -ffast-math -ftree-vectorize -fopenmp -std=gnu++20 -o radix-sort src/main.cpp

clean:
	rm -f radix-sort run.* core.*

run: clean radix-sort
	qrun 20c 1 pdp_fast run

run-long: clean radix-sort
	qrun 20c 1 pdp_serial run
