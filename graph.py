#!/usr/bin/env python3
import re

from sys import argv
from glob import glob
from pprint import pprint

from matplotlib import pyplot as plt


def read(folder, type_):
    files = glob(f'./{folder}/*.o*')
    labels, init, count, sum_, order = ([] for i in range(5))
    for file in sorted(files, key=lambda x: re.search('run(\d+).o.*', x).group(1)):
        print('>>', file)
        n = int(re.search('run(\d+).o.*', file).group(1))
        # labels.append(f'{n}')
        labels.append(f'{type_} {n}')
        with open(file) as f:
            for line in f:
                if line.startswith(f'{type_} cummulative'):
                    times = tuple(map(float, line.split()[2:]))
                    init.append(times[0])
                    count.append(times[1])
                    sum_.append(times[2])
                    order.append(times[3])
    return labels, init, count, sum_, order


def plot(ax, labels, init, count, sum_, order):
    ax.bar(labels, init, label='Initialization', color='red')
    ax.bar(labels, count, label='Counting', bottom=init, color='green')
    ax.bar(
        labels,
        sum_,
        label='Prefix scan',
        bottom=list(map(sum, zip(init, count))),
        color='blue',
    )
    ax.bar(
        labels,
        order,
        label='Ordeding',
        bottom=list(map(sum, zip(init, count, sum_))),
        color='orange',
    )


fig, ax = plt.subplots(1, 1, figsize=(10, 5))
plot(ax, *read(argv[1], 'seq'))
plot(ax, *read(argv[1], 'par'))

ax.set_ylabel('time in seconds')
ax.set_xlabel('radix size')
# ax.set_xlabel('number of threads')
# ax.set_xlabel('number of elements')

ax.legend()
handles, labels = plt.gca().get_legend_handles_labels()
by_label = dict(zip(labels, handles))
plt.legend(by_label.values(), by_label.keys())
plt.setp(
    ax.get_xticklabels(),
    rotation=30,
    horizontalalignment='right',
)
plt.subplots_adjust(bottom=0.25)
plt.savefig('plot.png')
