# Parallel Radix Sort

Our task is to sort numbers in ascending order. To accomplish this task, we will use the radix sorting algorithm. Radix sort is a sorting algorithm that does not use comparison to order the elements. Due to this fact, it is not bounded by $\Omega(n * log(n))$, where $n$ is the size of the sequence.

## Sequential algorithm description

Radix sort is a stable sort. This is an essential feature of the sort since it sorts the numbers in several passes. It represents the numbers as a binary and splits them into chunks of equal sizes. The size of each chunk is determined by radix size. The radix size determines the number of bits that will be used for sorting. For each chunk, it sorts the numbers by the numerical value of the chunk. It uses a counting sort for each pass.

The counting sort consists of three phases. Initially, it creates a vector where it will store the number of occurrences of the chunks. The size of this vector is $2^{radix\ size}$. In the first phase, it counts the occurrences of numbers with the same numerical value as the chunk. In the second phase, it computes an inclusive prefix sum from the counts to find out the cumulative number of chunks. In the last phase, it positions the numbers to the positions computed by the previous step.

## Parallelization of sequential algorithm

Since the counting sort passes are dependent on one another, we need to parallelize the counting sort. There is a data dependence between the three phases of counting sort, therefore, we try to parallelize them separately.

During the initialization, we create a vector for counting occurrences for every thread. Each thread then iterates over a dedicated part of the input vector and adds respective counts. This loop cannot be vectorized since the indices to the vector of counts are computed from the numerical value of the chunks.

To speed up the second phase, we utilize `omp scan` along with `omp simd reduction` directives. This phase will be vectorized, after it finishes, each thread will have stored the positions where to place numbers in the output vector.

In the final phase, each thread knows where to place the numbers from its part of the vector. Because every thread has a dedicated part of the output vector, there are no data races and thus, these operations can run in parallel. The parallelization is not possible since the indices to the output vector are dynamically computed from the vector of summed counts.

## Measurements

We made two implementations of parallel radix sort. The first one has a false sharing issue, and the second one does not. We decided to include the measurements from both of those implementations since it may be interesting to compare them. Let us look at the first version with the false sharing issue.

### Version 1

We ran three types of measurements. Firstly, we fixed the vector size and changed the size of the radix.

![Radix size vs. time in seconds (vector size = $10^8$, threads = 30)](out/graphs/v1/radix-size.png)
\

As we can see, the parallel versions of the algorithm did not perform well. The smaller the radix the greater the slowdown. This slowdown occurred because when the radix is small, the vector for storing occurrences is small as well. This means that parts of the vector, that will be processed by different threads end up in the same cache line, and thus a false sharing occurs. When the radix size was lager, the vector size was larger, and therefore, there was a smaller chance of false sharing.

Then we compared how the computation time changes when we increase the number of threads. We chose the radix size 16 since it performed the best in previous measurements and it does not suffer from false sharing.

![Number of threads vs. time in seconds (radix size = 16, vector size = $10^9$)](out/graphs/v1/threads.png)
\

The time decreases as we increase the number of threads up to 8. Then it stabilizes.

Finally, we compare how the input vector size affects the computation time. Again, we choose the radix size 16, and the maximum number of threads = 30.

![Size of vector vs. time in seconds (radix size = 16, threads = 30)](out/graphs/v1/vector-size.png)
\

As expected, the parallel algorithm is effective only for larger input sizes. In the case of the vector size $10^9$ the time decreased to a $1/3$ of the sequential solution.

### Version 2

As we have seen, the parallel algorithm did not perform well when the radix size was smaller than 16 because of false sharing. To fix this issue, we introduced dummy data to enlarge the vector of occurrences and thus patch the false sharing.

We measured the same dependencies as in the previous version.

![Radix size vs. time in seconds (vector size = $10^8$, threads = 30)](out/graphs/v2/radix-size.png)
\

Here we can see that the false sharing was indeed fixed, and now every parallel solution is faster than its corresponding sequential one.

![Radix size vs. time in seconds (vector size = $10^9$, threads = 30)](out/graphs/v2/radix-size-large.png)
\

Thanks to the dummy data, the algorithms were fast enough, so we increased the vector size by a factor of 10. As we may observe, the ratio between the times of different radix sizes stays the same.

![Number of threads vs. time in seconds (radix size = 16, vector size = $10^9$)](out/graphs/v2/threads.png)
\

We can observe a speedup as we increase the number of threads, however, it is not as substantial as in version one. The cause may that we chose the radix size 16, which does not need the dummy data to fix the false sharing. Therefore, the dummy data cause a slowdown.

![Size of vector vs. time in seconds (radix size = 16, threads = 30)](out/graphs/v2/vector-size.png)
\

The second version scales similarly to the first one. We chose the radix size 16, so the maximum speedup is not as large as in version one due to the reasons we described above.

## Conclusion

We were able to program a radix sort that utilizes vectorization and parallelization. However, one needs to pay attention to false sharing because it may become a source of great inefficiency. After fixing the false sharing issue, we can see that the parallel version of the program ran significantly faster. The best-performing option was to use a radix size of 16 and not utlize false sharing precautions.

## Building

This project uses the `cmake` build system. After running the following commands, the executable will be located in the `build` directory.

```sh
cmake -B build
make -C build
```

## Running

```sh
./radix-sort
usage: ./radix-sort <sort-type:all|1|2|4|8|16> <seed:int=random>
                    <size:int=16> <max-threads:int=nproc>
```
