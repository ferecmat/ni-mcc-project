import os

i = 100
while i < 1000_000_000:
    cmd = f'./build/radix-sort x 1 {i} > ./out/seq/{i}'
    print(cmd)
    os.system(cmd)
    i *= 10
